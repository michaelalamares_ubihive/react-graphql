import React from 'react'

const Snackbar = ({ title, snackbarShow, type }) => {

  return (
    <div className={`snackbar ${snackbarShow ? `show ${type}` : ''}`}>
      {title}
    </div>
  )
}

export default Snackbar