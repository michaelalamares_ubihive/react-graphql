import React from 'react'
import { FaArrowCircleRight, FaPen, FaTrash } from "react-icons/fa";
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_MOVIE, REMOVE_MOVIE, GET_MOVIES } from '../queries/queries'

const Movie = (props) => {

  const {
    title, 
    toggleModal, 
    movieID, 
    setSelectedMovie,
    setMovieID,
    setTitle,
    setDescription,
    setWorth,
    setGenre,
    setFormState,
    showSnackbar
  } = props
  
  const variables = {
    variables: {
      movieID: movieID
    }
  }

  const removeVar = {
    movieID: movieID
  }
  
  const { data } = useQuery(GET_MOVIE, variables);

  const [removeMovie] = useMutation(REMOVE_MOVIE)

  const handleClick = () => {
    toggleModal()
    setSelectedMovie(movieID)
  }

  const handleEditClick = () => {
    if (data) {
      setMovieID(movieID)
      setTitle(data.MovieOne.title)
      setDescription(data.MovieOne.description)
      setGenre(data.MovieOne.genre)
      setWorth(data.MovieOne.worth)
      setFormState('edit')
    }
  }

  const handleRemoveClick = () => {
    removeMovie({
      variables: removeVar,
      refetchQueries: [{ query: GET_MOVIES }]
    }).then(doc => {
      showSnackbar('Movie deleted successfully', 'success')
    })
    
  }

  return (
    <li className="movie">
      {title}
      <div className="icon-wrapper">
        <FaTrash onClick={handleRemoveClick}/>
        <FaPen onClick={handleEditClick} />
        <FaArrowCircleRight onClick={handleClick} />
      </div>
    </li>
  )
}

export default Movie