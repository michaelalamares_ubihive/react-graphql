import React from 'react'
import { FaTimes } from 'react-icons/fa'
import { useQuery } from '@apollo/react-hooks';
import { GET_MOVIE } from '../queries/queries'

const Modal = ({ modalShow, toggleModal, movieID }) => {
  const variables = {
    variables: {
      movieID: movieID
    }
  }

  const { data, refetch } = useQuery(GET_MOVIE, variables)

  const displayMovie = () => {
    refetch();
    
    if (data) {
      return (
        <div>
          <h2>{data.MovieOne.title}</h2>
          <p>{data.MovieOne.description}</p>
          <div className="movie-info">
            <div>
              <span className="label">Date Release:</span> <span className="label-val">{data.MovieOne.releasedDate}</span>
            </div>
            <div>
              <span className="label">Directed by:</span> <span className="label-val">{data.MovieOne.director.name}</span>
            </div>
            <div>
              <span className="label">Genre:</span> <span className="label-val">{data.MovieOne.genre}</span>
            </div>
            <div>
              <span className="label">&#8369;</span> <span className="label-val last">{data.MovieOne.worth}</span>
            </div>
          </div>
        </div>
      )
    } else {
      return (<div>Loading movies....</div>)
    }
  }

  return (
    <div className={`modal-wrapper ${modalShow ? 'show': ''}`}>
      <div className="modal">
        <button onClick={toggleModal}><FaTimes/></button>
        { displayMovie() }
      </div>
    </div>
  )
}

export default Modal