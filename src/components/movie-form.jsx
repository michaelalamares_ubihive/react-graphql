import React from 'react'
import { IconContext } from "react-icons";
import { FaPlusCircle, FaCheckCircle, FaTimesCircle } from 'react-icons/fa'
import { useMutation } from '@apollo/react-hooks';
import { ADD_MOVIE, GET_MOVIES, UPDATE_MOVIE } from '../queries/queries'

const MovieForm = (props) => {

  const {
    movieID,
    title,
    setTitle,
    description,
    setDescription,
    worth,
    setWorth,
    genre,
    setGenre,
    formState,
    setFormState,
    showSnackbar
  } = props

  const cleanForm = () => {
    setFormState('add')
    setTitle('')
    setDescription('')
    setWorth('')
    setGenre('')
  }

  const [addMovie] = useMutation(ADD_MOVIE);
  const [updateMovie] = useMutation(UPDATE_MOVIE)
  
  const handleClickSubmit = e => {
    e.preventDefault()

    if (e.target.tagName === 'path' || e.target.tagName === 'svg') {
      if (e.target.parentElement.classList.contains('btn-add')) {
        addMovie({ 
          variables: { 
            title: title,
            description: description,
            worth: parseFloat(worth),
            genre: parseFloat(genre)
          },
          refetchQueries: [{ query: GET_MOVIES }]
        })
          .then(doc => {
            cleanForm()
            showSnackbar(`Movie ${doc.data.MovieCreate.record.title} added.`, 'success')
          })
      } else if (e.target.parentElement.classList.contains('btn-save')) {

        updateMovie({
          variables: {
            movieID: movieID,
            title: title,
            description: description,
            worth: parseFloat(worth),
            genre: parseFloat(genre)
          },
          refetchQueries: [{ query: GET_MOVIES }]
        })
          .then(doc => {
            cleanForm()
            showSnackbar('Movie updated successfully', 'success')
          })

      } else if (e.target.parentElement.classList.contains('btn-cancel')) {
        cleanForm()
      }
    }
    

    
  }

  return (
    <div className="movie-frm-container">
      <h1>{formState === 'add' ? 'Add' : 'Update'} Movie</h1>
      <form className="moviep-frm" onClick={handleClickSubmit}>
        <input type="text" placeholder="Title" onChange={e => setTitle(e.target.value)} value={title}></input>
        <input type="text" placeholder="Description" onChange={e => setDescription(e.target.value)} value={description}></input>
        <input type="number" placeholder="Worth" onChange={e => setWorth(e.target.value)} value={worth}></input>
        <input type="number" placeholder="Genre" onChange={e => setGenre(e.target.value)} value={genre}></input>
        {
          formState === 'add' ? 
            <button type="submit" className="btn-add">
              <IconContext.Provider value={{ className: "btn-add" }}>
                <div className="btn-add">
                  <FaPlusCircle/>
                </div>
              </IconContext.Provider>
            </button> :
          <div className="btn-wrapper">
            <button type="submit" className="btn-cancel">
              <IconContext.Provider value={{ className: "btn-cancel" }}>
                <div className="btn-cancel">
                  <FaTimesCircle/>
                </div>
              </IconContext.Provider>
            </button> 
            <button type="submit" className="btn-save">
              <IconContext.Provider value={{ className: "btn-save" }}>
                <div className="btn-save">
                  <FaCheckCircle/>
                </div>
              </IconContext.Provider>
            </button>
          </div>
        }
      </form>
    </div>
  )
}

export default MovieForm

