import React from 'react'
import { GET_MOVIES } from '../queries/queries'
// import { graphql } from 'graphql'
import { useQuery } from '@apollo/react-hooks';
import Movie from './movie'

const MovieList = (props) => {
  const {
    toggleModal, 
    setSelectedMovie,
    setMovieID,
    setTitle,
    setDescription,
    setWorth,
    setGenre,
    setFormState,
    showSnackbar
   } = props
  const { loading, data } = useQuery(GET_MOVIES);
  
  const displayMovies = () => {
    if (loading) {
      return (<div>Loading movies....</div>)
    } else {
      return data.MovieMany.map(movie => <Movie 
        key={movie._id} 
        setSelectedMovie={setSelectedMovie} 
        toggleModal={toggleModal} 
        movieID={movie._id} 
        title={movie.title} 
        setMovieID={setMovieID}
        setTitle={setTitle}
        setDescription={setDescription}
        setWorth={setWorth}
        setGenre={setGenre}
        setFormState={setFormState}
        showSnackbar={showSnackbar}
        />)
    }
  }
  
  return (
    <ul className="movie-list">
      {displayMovies()}
    </ul>
  )
}

export default MovieList