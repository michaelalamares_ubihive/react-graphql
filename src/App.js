import React, { useState } from 'react';
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'
import './App.css';
import MovieList from './components/movie-list'
import MovieForm from './components/movie-form'
import Modal from './components/modal'
import Snackbar from './components/snackbar'

const client = new ApolloClient({
  uri: 'http://192.168.0.103:7000/graphql'
})

function App() {

  const [modalShow, setModalShow] = useState(false)
  const [selectedMovie, setSelectedMovie] = useState({})

  const showSnackbar = (title, type) => {
    setSnackbar({ snackbarShow: true, title: title, type: type })

    setTimeout(() => setSnackbar({ title: title, type: type, snackbarShow: false }), 2500)
  }

  const toggleModal = () => setModalShow(!modalShow)
  const [snackbar, setSnackbar] = useState({ snackbarShow: false, title: '', type: ''})
  
  
  const [formState, setFormState] = useState('add')
  const [movieID, setMovieID] = useState('')
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [worth, setWorth] = useState('')
  const [genre, setGenre] = useState('')

  // const _setFormState = (value) => {
  //   setFormState(value)
  // }

  return (
    <ApolloProvider client={client}>
      <div className="App">
        <MovieForm 
          movieID={movieID}
          setMovieID={setMovieID}
          title={title}
          setTitle={setTitle}
          description={description}
          setDescription={setDescription}
          worth={worth}
          setWorth={setWorth}
          genre={genre}
          setGenre={setGenre}
          formState={formState}
          setFormState={setFormState}
          showSnackbar={showSnackbar}
        />
        <div className="movies">
          <h1 onClick={() => setFormState('edit')}>Movie List</h1>
          <MovieList 
            toggleModal={toggleModal} 
            setSelectedMovie={setSelectedMovie}
            setMovieID={setMovieID} 
            setTitle={setTitle}
            setDescription={setDescription}
            setWorth={setWorth}
            setGenre={setGenre}
            setFormState={setFormState}
            showSnackbar={showSnackbar}
          />
        </div>
        <Modal movieID={selectedMovie} modalShow={modalShow} toggleModal={toggleModal} />
        <Snackbar title={snackbar.title}  snackbarShow={snackbar.snackbarShow} type={snackbar.type} />
      </div>
    </ApolloProvider>
  );
}

export default App;
