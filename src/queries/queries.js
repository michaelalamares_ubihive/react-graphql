import { gql } from 'apollo-boost'

export const GET_MOVIES = gql`
  query {
    MovieMany(sort: _ID_DESC) {
      title
      _id
    }
  }
`

export const ADD_MOVIE = gql`
  mutation($title: String!, $description: String!, $genre: Float!, $worth: Float!) {
    MovieCreate(record: { 
        title: $title, 
        description: $description,
        releasedDate: "2019-08-29",
        genre: $genre,
        worth: $worth,
        director: "5d9c4bd74d6746562e4a2686",
        casts: {
          director: "direct1",
          producer: "producer1",
          leadActor: "lead1",
          support: "support1"
        }
        
    }) {
      record {
        title
        description
      }
    }
  }
`
export const GET_MOVIE = gql`
  query($movieID: MongoID!) {
    MovieOne(filter: {
      _id: $movieID
    }) {
      title
      description
      releasedDate
      genre
      worth
      director {
        name
      }
    }
  }
`

export const UPDATE_MOVIE = gql`
  mutation($movieID: MongoID!, $title: String!, $description: String!, $genre: Float!, $worth: Float!) {
    MovieUpdateById(record: {
      _id: $movieID,
      title: $title,
      description: $description,
      genre: $genre,
      worth: $worth
    }) {
      record {
        title
        description
      }
    }
  }
`
export const REMOVE_MOVIE = gql`
  mutation($movieID: MongoID!) {
    MovieRemoveById(_id: $movieID) {
      record {
        title
      }
    }
  }
`